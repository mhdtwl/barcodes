from dataclasses import dataclass


@dataclass
class Order:
    key: int
    customer_id: int


@dataclass
class Barcode:
    key: int
    order_id: int | None


@dataclass
class CustomerVoucher:
    customer_id: int
    order_id: int
    barcodes: list
