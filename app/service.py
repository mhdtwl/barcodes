import sys
from collections import Counter
from itertools import groupby
from typing import Any

from .entities import Barcode, CustomerVoucher, Order

ORDER_ID_CONST = "order_id"
CUSTOMER_ID_CONST = "customer_id"
BARCODE_CONST = "barcode"


class BarcodeFinderService:
    """Reads and parses barcodes, orders and customers."""

    @classmethod
    def build_orders(cls, orders_dict_list: list) -> list:
        orders = []
        for dict_order in orders_dict_list:
            orders.append(
                Order(
                    key=dict_order[ORDER_ID_CONST],
                    customer_id=dict_order[CUSTOMER_ID_CONST],
                )
            )
        return orders

    @classmethod
    def build_barcodes(cls, barcodes_dict_list: list) -> list:
        barcodes = []
        for dict_barcode in barcodes_dict_list:
            cls.validate_barcode(dict_barcode, barcodes)
            barcodes.append(
                Barcode(
                    key=dict_barcode[BARCODE_CONST],
                    order_id=dict_barcode[ORDER_ID_CONST],
                )
            )
        return barcodes

    @classmethod
    def send_error(cls, message: str):
        print(message, file=sys.stderr)

    @classmethod
    def search_by_key(cls, data_list: list, key):
        for element in data_list:
            if element.key == key:
                return element
        return None

    @classmethod
    def item_exists(cls, data_list: list, key: int) -> bool:
        if cls.search_by_key(data_list, key):
            return True
        return False

    @classmethod
    def validate_barcode(cls, barcode_dict: dict, barcode_list: list) -> None:
        barcode_key = barcode_dict[BARCODE_CONST]
        if cls.item_exists(barcode_list, barcode_key):
            cls.send_error(
                "InputValidationError: Barcode " + barcode_key + " exists!!"
            )  # No duplicate barcodes

    @classmethod
    def get_barcode_by_order_id(cls, order_id: int, barcode_list: list) -> list[str]:
        codes = []
        for barcode_obj in barcode_list:
            if barcode_obj.order_id and barcode_obj.order_id == order_id:
                codes.append(
                    barcode_obj.key
                )  # No orders without barcodes, They will be in unsold list
        return codes

    @classmethod
    def get_customer_vouchers(
            cls, orders: list, sold_barcodes: list
    ) -> list[CustomerVoucher]:
        customer_vouchers = []
        orders = sorted(orders, key=lambda x: int(x.customer_id))
        for order in orders:
            order_barcodes = cls.get_barcode_by_order_id(order.key, sold_barcodes)
            if order_barcodes:
                customer_vouchers.append(
                    CustomerVoucher(
                        customer_id=order.customer_id,
                        order_id=order.key,
                        barcodes=order_barcodes,
                    )
                )

        return customer_vouchers

    @classmethod
    def get_unsold_barcodes(cls, barcode_list: list):
        unsold_barcodes = []
        for barcode_obj in barcode_list:
            if not barcode_obj.order_id:
                unsold_barcodes.append(barcode_obj)
        return unsold_barcodes

    @classmethod
    def get_sold_barcodes(cls, barcode_list: list):
        sold_barcodes = []
        for barcode_obj in barcode_list:
            if barcode_obj.order_id:
                sold_barcodes.append(barcode_obj)
        return sold_barcodes

    @classmethod
    def count_orders_barcode(cls, order_id: int, sold_barcodes: list):
        for order_barcode in sold_barcodes:
            if order_id == order_barcode.order_id:
                # logging.log(str(len(cls.get_barcode_by_order_id(order_id, sold_barcodes))))
                return len(cls.get_barcode_by_order_id(order_id, sold_barcodes))
        return 0

    @classmethod
    def count_barcodes_by_customer(
            cls,
            customer_id: int,
            sold_barcodes: list,
            customer_voucher_list: list[CustomerVoucher],
    ) -> list[dict[str, int | Any]] | Any:
        customers_tickets = []
        for customer_voucher in customer_voucher_list:
            order_id = customer_voucher.order_id
            if customer_voucher.customer_id == customer_id:
                count_barcodes = cls.count_orders_barcode(order_id, sold_barcodes)
                customers_tickets.append(
                    {
                        "customer_id": customer_id,
                        "order_id": order_id,
                        "amount_of_tickets": count_barcodes,
                    }
                )

        result = [
            {
                "customer_id": key,
                "amount_of_tickets": sum(item["amount_of_tickets"] for item in group),
            }
            for key, group in groupby(customers_tickets, key=lambda x: x["customer_id"])
        ]

        return result[0]["amount_of_tickets"]

    @classmethod
    def get_top5_customers_buying(cls, customer_vouchers):
        voucher_counts = Counter(customer.customer_id for customer in customer_vouchers)
        return voucher_counts.most_common(5)
