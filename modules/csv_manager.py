import csv


class CsvManager:
    """Reads and parses csv files."""

    @classmethod
    def read_csv_with_headers(cls, file_path):
        data = []
        with open(file_path, "r", newline="") as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                data.append(row)
        return data

    @classmethod
    def write_combined_csv_with_headers(cls, file_path, data, fieldnames):
        with open(file_path, "w", newline="") as csvfile:
            writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
            writer.writeheader()
            writer.writerows(data)
