"""
   This file used as command to generate voucher csv file
"""

import os
from app.service import BarcodeFinderService
from modules.csv_manager import CsvManager

print("You working directory must be under /barcodes folder, you current pwd is: ")
print(os.getcwd())
#
BARCODE_FILE = "data/barcodes.csv"
ORDERS_FILE = "data/orders.csv"
VOUCHER_FILE = "data/vouchers-output.csv"
#
csv_manager = CsvManager()
service = BarcodeFinderService()
#
csv_orders = csv_manager.read_csv_with_headers(ORDERS_FILE)
csv_barcodes = csv_manager.read_csv_with_headers(BARCODE_FILE)
#
# order_header = csv_orders[0].keys() | barcode_header = csv_barcodes[0].keys()
#
orders = service.build_orders(csv_orders)
barcodes = service.build_barcodes(csv_barcodes)

sold_barcodes = service.get_sold_barcodes(barcodes)
unsold_barcodes = service.get_unsold_barcodes(barcodes)

# solution1
customer_vouchers = service.get_customer_vouchers(orders, sold_barcodes)
voucher_list = []
for cv in customer_vouchers:
    # logger.info( str(cv.customer_id) + ',' + str(cv.order_id) + ',' + str(','.join(cv.barcodes)))
    voucher_obj = {
        "customer_id": str(cv.customer_id),
        "order_id": str(cv.order_id),
        "barcodes": ",".join(cv.barcodes),
    }
    voucher_list.append(voucher_obj)

csv_manager.write_combined_csv_with_headers(
    VOUCHER_FILE, voucher_list, ["customer_id", "order_id", "barcodes"]
)
print("You voucher file exported into: " + VOUCHER_FILE)

# Bonus requests
print("_____________")
top5_customers_bought_orders = service.get_top5_customers_buying(customer_vouchers)
print("Top 5 customer of buying orders:")
print("customer_id, number_of_orders, number_of_tickets/barcodes")
for customer_order in top5_customers_bought_orders:
    customer_id = customer_order[0]
    no_orders = customer_order[1]
    no_tickets = service.count_barcodes_by_customer(
        customer_id, sold_barcodes, customer_vouchers
    )
    print(str(customer_id) + ", " + str(no_orders) + ", " + str(no_tickets))

print("_____________")
print(
    "The amount of unused barcodes:" + str(len(unsold_barcodes))
)  # for code in unsold_barcodes: print(code.key)
