# Barcodes / Vouchers

ERD
![img.png](img.png)

## Collaborate
maltawil

## Test and Deploy
Deploy to K8s or directly to a server 


## Installation
Install python 3.12, you can use the venv to run it for development purpose
don't forget then to run black, isort and pylint on your code.

>	python3 -m black .
>
>	python3 -m isort .
>
>	python3 -m pylint --recursive=yes --disable=missing-docstring app make.py


## Usage
Make sure you have the 2 files barcodes.csv, orders.csv under data folder 
they are set as constants in make.py and the output file will be vouchers-output.csv after you run below command
> python3 ./make.py